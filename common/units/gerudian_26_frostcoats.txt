type = infantry
unit_type = tech_gerudian

maneuver = 1
offensive_morale = 3
defensive_morale = 4
offensive_fire = 4
defensive_fire = 3
offensive_shock = 2
defensive_shock = 4

trigger = {
	NOT = {
		primary_culture = grombar_half_orc
		primary_culture = grombar_orc
		primary_culture = gray_orc
	}
}