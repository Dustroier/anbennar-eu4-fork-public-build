artifice_invention_basic_template = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		always = no
	}
	can_select = {
		artifice_has_points_for_basic = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_basic = yes
		#update_artifice_points = yes #is included in the above effect
	}
	on_revoked = {
		artifice_remove_basic = yes
		#update_artifice_points = yes #is included in the above effect
	}
	penalties = {
		#foo
	}
	benefits = {
		#foo
	}
	ai_will_do = {
		factor = 5
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_expert_template = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		always = no
	}
	can_select = {
		artifice_has_points_for_expert = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_expert = yes
		#update_artifice_points = yes #is included in the above effect
	}
	on_revoked = {
		artifice_remove_expert = yes
		#update_artifice_points = yes #is included in the above effect
	}
	penalties = {
		#foo
	}
	benefits = {
		#foo
	}
	ai_will_do = {
		factor = 25
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_advanced_template = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		always = no
	}
	can_select = {
		artifice_has_points_for_advanced = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_advanced = yes
		#update_artifice_points = yes #is included in the above effect
	}
	on_revoked = {
		artifice_remove_advanced = yes
		#update_artifice_points = yes #is included in the above effect
	}
	penalties = {
		#foo
	}
	benefits = {
		#foo
	}
	ai_will_do = {
		factor = 50
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_masterwork_template = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		always = no
	}
	can_select = {
		artifice_has_points_for_masterwork = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_masterwork = yes
		#update_artifice_points = yes #is included in the above effect
	}
	on_revoked = {
		artifice_remove_masterwork = yes
		#update_artifice_points = yes #is included in the above effect
	}
	penalties = {
		#foo
	}
	benefits = {
		#foo
	}
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

#actual in-game privileges start here

artifice_invention_research_privilege = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		always = yes
	}
	can_select = {
		artifice_has_points_for_basic = yes
		is_year = 1445
		NOT = {
			has_estate_privilege = estate_artificers_organization_state
			has_estate_privilege = artifice_invention_directed_research
		}
	}
	can_revoke = {
		always = no
	}
	on_granted = {
		artifice_granted_basic = yes
		country_event = {
			id = artifice_events.1
		}
	}
	on_revoked = {
		artifice_remove_basic = yes
		#update_artifice_points = yes #is included in the above effect
		#custom_tooltip = decreases_our_artifice_upkeep_by_10
	}
	penalties = {
		prestige_decay = 0.02
		global_tax_income = -24
	}
	benefits = {
		#foo
	}
	cooldown_years = 10
	on_cooldown_expires = {
		country_event = {
			id = artifice_events.2
		}
	}
	ai_will_do = {
		factor = 0 #ai shouldn't use this one. Just too much work getting them to understand it and making it not crash for linux. Their research will be done via decision?
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_directed_research = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_estate_privilege = estate_artificers_organization_state
	}
	can_select = {
		artifice_has_points_for_basic = yes
		is_year = 1445
		NOT = {
			has_estate_privilege = artifice_invention_research_privilege
		}
	}
	can_revoke = {
		always = no
	}
	on_granted = {
		artifice_granted_basic = yes
		country_event = {
			id = artifice_events.4
		}
	}
	on_revoked = {
		artifice_remove_basic = yes
		#update_artifice_points = yes #is included in the above effect
		#custom_tooltip = decreases_our_artifice_upkeep_by_10
	}
	penalties = {
		prestige_decay = 0.02
		global_tax_income = -24
	}
	benefits = {
		#foo
	}
	cooldown_years = 10
	on_cooldown_expires = {
		country_event = {
			id = artifice_events.3
		}
	}
	ai_will_do = {
		factor = 0 #ai shouldn't use this one. Just too much work getting them to understand it and making it not crash for linux. Their research will be done via decision?
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

#inventions

artifice_invention_portable_turrets = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = unlocked_artifice_invention_portable_turrets
	}
	can_select = {
		artifice_has_points_for_basic = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_basic = yes
		#update_artifice_points = yes #is included in the above effect
		add_country_modifier = {
			name = artifice_portable_turrets
			duration = -1
		}
	}
	on_revoked = {
		artifice_remove_basic = yes
		#update_artifice_points = yes #is included in the above effect
		remove_country_modifier = artifice_portable_turrets
	}
	penalties = {
		#foo
		movement_speed = -0.1
	}
	benefits = {
		#foo
		siege_ability = 0.1
	}
	ai_will_do = {
		factor = 5
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_sparkdrive_rifles = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = unlocked_artifice_invention_sparkdrive_rifles
	}
	can_select = {
		artifice_has_points_for_basic = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_basic = yes
		#update_artifice_points = yes #is included in the above effect
		add_country_modifier = {
			name = artifice_sparkdrive_rifles
			duration = -1
		}
	}
	on_revoked = {
		artifice_remove_basic = yes
		#update_artifice_points = yes #is included in the above effect
		remove_country_modifier = artifice_sparkdrive_rifles
	}
	penalties = {
		#foo
	}
	benefits = {
		#foo
	}
	ai_will_do = {
		factor = 5
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_artificer_exo_arms = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = unlocked_artifice_invention_artificer_exo_arms
	}
	can_select = {
		artifice_has_points_for_basic = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_basic = yes
		#update_artifice_points = yes #is included in the above effect
		add_country_modifier = {
			name = artifice_exo_arms
			duration = -1
		}
	}
	on_revoked = {
		artifice_remove_basic = yes
		#update_artifice_points = yes #is included in the above effect
		remove_country_modifier = artifice_exo_arms
	}
	penalties = {
		#foo
	}
	benefits = {
		#foo
	}
	conditional_modifier = {
        trigger = {
            always = yes
        }

    }
	ai_will_do = {
		factor = 5
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_viewcatcher = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = unlocked_artifice_invention_viewcatcher
	}
	can_select = {
		artifice_has_points_for_basic = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_basic = yes
		#update_artifice_points = yes #is included in the above effect
	}
	on_revoked = {
		artifice_remove_basic = yes
		#update_artifice_points = yes #is included in the above effect
	}
	penalties = {
		#foo
	}
	benefits = {
		country_admin_power = 1
	}
	ai_will_do = {
		factor = 5
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_vendorless_stall = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = unlocked_artifice_invention_vendorless_stall
	}
	can_select = {
		artifice_has_points_for_basic = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_basic = yes
		#update_artifice_points = yes #is included in the above effect
	}
	on_revoked = {
		artifice_remove_basic = yes
		#update_artifice_points = yes #is included in the above effect
	}
	penalties = {
		#foo
	}
	benefits = {
		global_tax_modifier = 0.1
	}
	ai_will_do = {
		factor = 5
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_sending_stones = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = unlocked_artifice_invention_sending_stones
	}
	can_select = {
		artifice_has_points_for_basic = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_basic = yes
		#update_artifice_points = yes #is included in the above effect
	}
	on_revoked = {
		artifice_remove_basic = yes
		#update_artifice_points = yes #is included in the above effect
	}
	penalties = {
		#foo
	}
	benefits = {
		envoy_travel_time = -0.5
		diplomatic_upkeep = 1
	}
	ai_will_do = {
		factor = 5
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_vorpal_bullets = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = unlocked_artifice_invention_vorpal_bullets
	}
	can_select = {
		artifice_has_points_for_basic = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_basic = yes
		#update_artifice_points = yes #is included in the above effect
		add_country_modifier = {
			name = artifice_vorpal_bullets
			duration = -1
		}
	}
	on_revoked = {
		artifice_remove_basic = yes
		#update_artifice_points = yes #is included in the above effect
		remove_country_modifier = artifice_vorpal_bullets
	}
	penalties = {
		#foo
	}
	benefits = {
		#foo
	}
	ai_will_do = {
		factor = 5
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_war_golems = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = unlocked_artifice_invention_war_golems
	}
	can_select = {
		artifice_has_points_for_basic = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_basic = yes
		#update_artifice_points = yes #is included in the above effect
		add_country_modifier = {
			name = artifice_war_golems
			duration = -1
		}
	}
	on_revoked = {
		artifice_remove_basic = yes
		#update_artifice_points = yes #is included in the above effect
		remove_country_modifier = artifice_war_golems
	}
	penalties = {
		#foo
	}
	benefits = {
		#foo
	}
	ai_will_do = {
		factor = 5
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_black_damestear_bullets = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_institution = enlightenment
		has_country_flag = unlocked_artifice_invention_black_damestear_bullets
	}
	can_select = {
		artifice_has_points_for_basic = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_basic = yes
		#update_artifice_points = yes #is included in the above effect
	}
	on_revoked = {
		artifice_remove_basic = yes
		#update_artifice_points = yes #is included in the above effect
	}
	penalties = {
		#foo
	}
	benefits = {
		discipline = 0.025
		yearly_revolutionary_zeal = 1
	}
	ai_will_do = {
		factor = 5
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}


artifice_invention_brass_prosthesis = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = unlocked_artifice_invention_brass_prosthesis
	}
	can_select = {
		artifice_has_points_for_expert = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_expert = yes
		#update_artifice_points = yes #is included in the above effect
	}
	on_revoked = {
		artifice_remove_expert = yes
		#update_artifice_points = yes #is included in the above effect
	}
	penalties = {
		#foo
	}
	benefits = {
		manpower_recovery_speed = 0.10
		sailors_recovery_speed = 0.1
	}
	ai_will_do = {
		factor = 25
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_crierless_crier_device = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = unlocked_artifice_invention_crierless_crier_device
	}
	can_select = {
		artifice_has_points_for_expert = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_expert = yes
		#update_artifice_points = yes #is included in the above effect
	}
	on_revoked = {
		artifice_remove_expert = yes
		#update_artifice_points = yes #is included in the above effect
	}
	penalties = {
		#foo
	}
	benefits = {
		country_diplomatic_power = 1
	}
	ai_will_do = {
		factor = 25
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_spell_in_a_box = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = unlocked_artifice_invention_spell_in_a_box
		has_institution = enlightenment
	}
	can_select = {
		artifice_has_points_for_expert = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_expert = yes
		#update_artifice_points = yes #is included in the above effect
	}
	on_revoked = {
		artifice_remove_expert = yes
		#update_artifice_points = yes #is included in the above effect
	}
	penalties = {
		#foo
	}
	benefits = {
		prestige_decay = -0.02
	}
	ai_will_do = {
		factor = 25
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_t_wave_transceivers = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = unlocked_artifice_invention_t_wave_transceivers
	}
	can_select = {
		artifice_has_points_for_expert = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_expert = yes
		#update_artifice_points = yes #is included in the above effect
	}
	on_revoked = {
		artifice_remove_expert = yes
		#update_artifice_points = yes #is included in the above effect
	}
	penalties = {
		#foo
	}
	benefits = {
		country_military_power = 1
	}
	ai_will_do = {
		factor = 25
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_living_mirrors = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = unlocked_artifice_invention_living_mirrors
		has_institution = industrialization
	}
	can_select = {
		artifice_has_points_for_expert = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_expert = yes
		#update_artifice_points = yes #is included in the above effect
	}
	on_revoked = {
		artifice_remove_expert = yes
		#update_artifice_points = yes #is included in the above effect
	}
	penalties = {
		#foo
	}
	benefits = {
		global_unrest = -2
		num_accepted_cultures = 2
	}
	ai_will_do = {
		factor = 25
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_remedial_tinctures = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = unlocked_artifice_invention_remedial_tinctures
	}
	can_select = {
		artifice_has_points_for_expert = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_expert = yes
		#update_artifice_points = yes #is included in the above effect
	}
	on_revoked = {
		artifice_remove_expert = yes
		#update_artifice_points = yes #is included in the above effect
	}
	penalties = {
		#foo
	}
	benefits = {
		recover_army_morale_speed = 0.2
		adventurers_loyalty_modifier = 0.2
	}
	ai_will_do = {
		factor = 25
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}
artifice_invention_wandlocks = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = unlocked_artifice_invention_wandlocks
		has_institution = enlightenment
	}
	can_select = {
		artifice_has_points_for_expert = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_expert = yes
		#update_artifice_points = yes #is included in the above effect
		add_country_modifier = {
			name = artifice_wandlocks
			duration = -1
		}
	}
	on_revoked = {
		artifice_remove_expert = yes
		#update_artifice_points = yes #is included in the above effect
		remove_country_modifier = artifice_wandlocks
	}
	penalties = {
		#foo
	}
	benefits = {
		#foo
	}
	ai_will_do = {
		factor = 25
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_artillery_autoloader = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = unlocked_artifice_invention_artillery_autoloader
		has_institution = industrialization
	}
	can_select = {
		artifice_has_points_for_expert = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_expert = yes
		#update_artifice_points = yes #is included in the above effect
	}
	on_revoked = {
		artifice_remove_expert = yes
		#update_artifice_points = yes #is included in the above effect
	}
	penalties = {
		#foo
	}
	benefits = {
		artillery_power = 0.1
	}
	ai_will_do = {
		factor = 25
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}


artifice_invention_elemental_locomotives = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = unlocked_artifice_invention_elemental_locomotives
		has_institution = industrialization
	}
	can_select = {
		artifice_has_points_for_advanced = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_advanced = yes
		#update_artifice_points = yes #is included in the above effect
	}
	on_revoked = {
		artifice_remove_advanced = yes
		#update_artifice_points = yes #is included in the above effect
	}
	penalties = {
		#foo
	}
	benefits = {
		free_policy = 1
		movement_speed = 0.10
	}
	ai_will_do = {
		factor = 50
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_mechanim_workforce = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = unlocked_artifice_invention_mechanim_workforce
		has_institution = industrialization
	}
	can_select = {
		artifice_has_points_for_advanced = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_advanced = yes
		#update_artifice_points = yes #is included in the above effect
	}
	on_revoked = {
		artifice_remove_advanced = yes
		#update_artifice_points = yes #is included in the above effect
	}
	penalties = {
		#foo
	}
	benefits = {
		production_efficiency = 0.1
		global_trade_goods_size_modifier = 0.1
	}
	ai_will_do = {
		factor = 50
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_commercial_sky_galleons = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = unlocked_artifice_invention_commercial_sky_galleons
		has_institution = industrialization
	}
	can_select = {
		artifice_has_points_for_advanced = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_advanced = yes
		#update_artifice_points = yes #is included in the above effect
	}
	on_revoked = {
		artifice_remove_advanced = yes
		#update_artifice_points = yes #is included in the above effect
	}
	penalties = {
		#foo
	}
	benefits = {
		placed_merchant_power = 20
		global_tariffs = 0.5
	}
	ai_will_do = {
		factor = 50
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_mechanim_soldiers = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = unlocked_artifice_invention_mechanim_soldiers
		has_institution = industrialization
	}
	can_select = {
		artifice_has_points_for_advanced = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_advanced = yes
		#update_artifice_points = yes #is included in the above effect
		add_country_modifier = {
			name = artifice_mechanim_soldiers
			duration = -1
		}
	}
	on_revoked = {
		artifice_remove_advanced = yes
		#update_artifice_points = yes #is included in the above effect
		remove_country_modifier = artifice_mechanim_soldiers
	}
	penalties = {
		#foo
	}
	benefits = {
		#foo
	}
	ai_will_do = {
		factor = 50
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_prototype_tanks = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = unlocked_artifice_invention_prototype_tanks
		has_institution = industrialization
	}
	can_select = {
		artifice_has_points_for_advanced = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_advanced = yes
		#update_artifice_points = yes #is included in the above effect
	}
	on_revoked = {
		artifice_remove_advanced = yes
		#update_artifice_points = yes #is included in the above effect
	}
	penalties = {
		#foo
	}
	benefits = {
		cavalry_fire = 2
	}
	ai_will_do = {
		factor = 50
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_personal_mageshields = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = unlocked_artifice_invention_personal_mageshields
		has_institution = enlightenment
	}
	can_select = {
		artifice_has_points_for_advanced = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_advanced = yes
		#update_artifice_points = yes #is included in the above effect
		add_country_modifier = {
			name = artifice_personal_mageshields
			duration = -1
		}
	}
	on_revoked = {
		artifice_remove_advanced = yes
		#update_artifice_points = yes #is included in the above effect
		remove_country_modifier = artifice_personal_mageshields
	}
	penalties = {
		#foo
	}
	benefits = {
		#foo
	}
	ai_will_do = {
		factor = 50
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_military_sky_galleons = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = unlocked_artifice_invention_military_sky_galleons 
		has_institution = industrialization
	}
	can_select = {
		artifice_has_points_for_advanced = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_advanced = yes
		#update_artifice_points = yes #is included in the above effect
	}
	on_revoked = {
		artifice_remove_advanced = yes
		#update_artifice_points = yes #is included in the above effect
	}
	penalties = {
		#foo
	}
	benefits = {
		ship_durability = 0.2
		artillery_power = 0.10
	}
	ai_will_do = {
		factor = 50
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

#mission shit

artifice_invention_cavern_quarrying_allclan = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = allclan_industrial_cavern_quarrying
	}
	can_select = {
		artifice_has_points_for_expert = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_expert = yes
		#update_artifice_points = yes #is included in the above effect
		custom_tooltip = allclan_quarrying_tt
	}
	on_revoked = {
		artifice_remove_expert = yes
		#update_artifice_points = yes #is included in the above effect
	}
	penalties = {
		#foo
	}
	benefits = {
		#foo
	}
	ai_will_do = {
		factor = 25
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_rocket_rails_allclan = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = can_make_rocket_rails
	}
	can_select = {
		artifice_has_points_for_expert = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_expert = yes
		#update_artifice_points = yes #is included in the above effect
		custom_tooltip = allclan_rocket_rails_tt
	}
	on_revoked = {
		artifice_remove_expert = yes
		#update_artifice_points = yes #is included in the above effect
	}
	penalties = {
		#foo
	}
	benefits = {
		#foo
		build_time = -0.075
	}
	ai_will_do = {
		factor = 25
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_factory_farms_allclan = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = allclan_factory_farms
		#always = yes
	}
	can_select = {
		artifice_has_points_for_expert = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_expert = yes
		#update_artifice_points = yes #is included in the above effect
		custom_tooltip = allclan_factory_farm_tt
	}
	on_revoked = {
		artifice_remove_expert = yes
		#update_artifice_points = yes #is included in the above effect
	}
	penalties = {
		#foo
	}
	benefits = {
		#foo
		build_time = -0.075
	}
	ai_will_do = {
		factor = 25
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_artifice_mining_allclan = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = allclan_artifice_mining
	}
	can_select = {
		artifice_has_points_for_expert = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_expert = yes
		#update_artifice_points = yes #is included in the above effect
		custom_tooltip = allclan_artifice_mining_tt
	}
	on_revoked = {
		artifice_remove_expert = yes
		#update_artifice_points = yes #is included in the above effect
	}
	penalties = {
		#foo
	}
	benefits = {
		#foo
		build_time = -0.075
	}
	ai_will_do = {
		factor = 25
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

artifice_invention_hold_mega_industry_allclan = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_flag = allclan_hold_mega_industry
	}
	can_select = {
		artifice_has_points_for_advanced = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_advanced = yes
		#update_artifice_points = yes #is included in the above effect
		custom_tooltip = allclan_mega_industry_tt
	}
	on_revoked = {
		artifice_remove_expert = yes
		#update_artifice_points = yes #is included in the above effect
	}
	penalties = {
		#foo
	}
	benefits = {
		#foo
		build_time = -0.075
	}
	ai_will_do = {
		factor = 25
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}


artifice_invention_allclan_ultimate_weapon = {
	icon = privilege_promote_maratha_nobility
	max_absolutism = 0
	loyalty = 0.05
	influence = 0 #artificer privileges must never affect influence due to the fact influence affects points
	is_valid = {
		has_country_modifier = goblin_military
		OR = {
			has_country_modifier = allclan_the_ultimate_weapon_commando_jetback_harness
			has_country_modifier = allclan_the_ultimate_weapon_drilling_cannon_shells
			has_country_modifier = allclan_the_ultimate_weapon_land_mine_launcher
			has_country_modifier = allclan_the_ultimate_weapon_mega_crawler
			has_country_modifier = allclan_the_ultimate_weapon_the_boom_boom_boom_gun
			has_country_modifier = allclan_the_ultimate_weapon_rotary_artillery
			has_country_modifier = allclan_the_ultimate_weapon_goblin_guided_missile
			has_country_modifier = allclan_the_ultimate_weapon_nibbins_flamethrower
			has_country_modifier = allclan_the_ultimate_weapon_super_duper_boots
			has_country_modifier = allclan_the_ultimate_weapon_the_city_cracker_bomb
			has_country_modifier = allclan_the_ultimate_weapon_the_fear_bomb
			has_country_modifier = allclan_the_ultimate_weapon_the_many_booms_launcher
			has_country_modifier = allclan_the_ultimate_weapon_the_assault_machine
			has_country_modifier = allclan_the_ultimate_weapon_twenty_good_goblins
		}
	}
	can_select = {
		artifice_has_points_for_masterwork = yes
	}
	can_revoke = {
		always = yes
	}
	on_granted = {
		artifice_granted_masterwork = yes
		#update_artifice_points = yes #is included in the above effect
		if = {
			limit = {
				has_country_modifier = allclan_the_ultimate_weapon_commando_jetback_harness
			}
			add_country_modifier = {
				name = allclan_the_ultimate_weapon_commando_jetback_harness_deployment
				duration = -1
			}
		}
		if = {
			limit = {
				has_country_modifier = allclan_the_ultimate_weapon_drilling_cannon_shells
			}
			add_country_modifier = {
				name = allclan_the_ultimate_weapon_drilling_cannon_shells_deployment
				duration = -1
			}
		}
		if = {
			limit = {
				has_country_modifier = allclan_the_ultimate_weapon_land_mine_launcher
			}
			add_country_modifier = {
				name = allclan_the_ultimate_weapon_land_mine_launcher_deployment
				duration = -1
			}
		}
		if = {
			limit = {
				has_country_modifier = allclan_the_ultimate_weapon_mega_crawler
			}
			add_country_modifier = {
				name = allclan_the_ultimate_weapon_mega_crawler_deployment
				duration = -1
			}
		}
		if = {
			limit = {
				has_country_modifier = allclan_the_ultimate_weapon_the_boom_boom_boom_gun
			}
			add_country_modifier = {
				name = allclan_the_ultimate_weapon_the_boom_boom_boom_gun_deployment
				duration = -1
			}
		}
		if = {
			limit = {
				has_country_modifier = allclan_the_ultimate_weapon_rotary_artillery
			}
			add_country_modifier = {
				name = allclan_the_ultimate_weapon_rotary_artillery_deployment
				duration = -1
			}
		}
		if = {
			limit = {
				has_country_modifier = allclan_the_ultimate_weapon_goblin_guided_missile
			}
			add_country_modifier = {
				name = allclan_the_ultimate_weapon_goblin_guided_missile_deployment
				duration = -1
			}
		}
		if = {
			limit = {
				has_country_modifier = allclan_the_ultimate_weapon_nibbins_flamethrower
			}
			add_country_modifier = {
				name = allclan_the_ultimate_weapon_nibbins_flamethrower_deployment
				duration = -1
			}
		}
		if = {
			limit = {
				has_country_modifier = allclan_the_ultimate_weapon_super_duper_boots
			}
			add_country_modifier = {
				name = allclan_the_ultimate_weapon_super_duper_boots_deployment
				duration = -1
			}
		}
		if = {
			limit = {
				has_country_modifier = allclan_the_ultimate_weapon_the_city_cracker_bomb
			}
			add_country_modifier = {
				name = allclan_the_ultimate_weapon_the_city_cracker_bomb_deployment
				duration = -1
			}	
		}
		if = {
			limit = {
				has_country_modifier = allclan_the_ultimate_weapon_the_fear_bomb
			}
			add_country_modifier = {
				name = allclan_the_ultimate_weapon_the_fear_bomb_deployment
				duration = -1
			}
		}
		if = {
			limit = {
				has_country_modifier = allclan_the_ultimate_weapon_the_many_booms_launcher
			}
			add_country_modifier = {
				name = allclan_the_ultimate_weapon_the_many_booms_launcher_deployment
				duration = -1
			}
		}
		if = {
			limit = {
				has_country_modifier = allclan_the_ultimate_weapon_the_assault_machine
			}
			add_country_modifier = {
				name = allclan_the_ultimate_weapon_the_assault_machine_deployment
				duration = -1
			}
		}
		if = {
			limit = {
				has_country_modifier = allclan_the_ultimate_weapon_twenty_good_goblins
			}
			add_country_modifier = {
				name = allclan_the_ultimate_weapon_super_duper_boots_deployment
				duration = -1
			}
		}
	}
	on_revoked = {
		artifice_remove_expert = yes
		#update_artifice_points = yes #is included in the above effect
		remove_country_modifier = allclan_the_ultimate_weapon_commando_jetback_harness_deployment
		remove_country_modifier = allclan_the_ultimate_weapon_drilling_cannon_shells_deployment
		remove_country_modifier = allclan_the_ultimate_weapon_land_mine_launcher_deployment
		remove_country_modifier = allclan_the_ultimate_weapon_mega_crawler_deployment
		remove_country_modifier = allclan_the_ultimate_weapon_the_boom_boom_boom_gun_deployment
		remove_country_modifier = allclan_the_ultimate_weapon_rotary_artillery_deployment
		remove_country_modifier = allclan_the_ultimate_weapon_goblin_guided_missile_deployment
		remove_country_modifier = allclan_the_ultimate_weapon_nibbins_flamethrower_deployment
		remove_country_modifier = allclan_the_ultimate_weapon_super_duper_boots_deployment
		remove_country_modifier = allclan_the_ultimate_weapon_the_city_cracker_bomb_deployment
		remove_country_modifier = allclan_the_ultimate_weapon_the_fear_bomb_deployment
		remove_country_modifier = allclan_the_ultimate_weapon_the_many_booms_launcher_deployment
		remove_country_modifier = allclan_the_ultimate_weapon_the_assault_machine_deployment
		remove_country_modifier = allclan_the_ultimate_weapon_twenty_good_goblins_deployment
	}
	penalties = {
		#foo
		army_tradition_decay = 0.01
	}
	benefits = {
		#foo
	}
	ai_will_do = {
		factor = 500
		modifier = {
			factor = 0
			over_artifice_capacity = yes
		}
	}
}

